--
-- Built with,
--
--        ,gggg,
--       d8" "8I                         ,dPYb,
--       88  ,dP                         IP'`Yb
--    8888888P"                          I8  8I
--       88                              I8  8'
--       88        gg      gg    ,g,     I8 dPgg,
--  ,aa,_88        I8      8I   ,8'8,    I8dP" "8I
-- dP" "88P        I8,    ,8I  ,8'  Yb   I8P    I8
-- Yb,_,d88b,,_   ,d8b,  ,d8b,,8'_   8) ,d8     I8,
--  "Y8P"  "Y888888P'"Y88P"`Y8P' "YY8P8P88P     `Y8
--

-- This is a starter colorscheme for use with Lush,
-- for usage guides, see :h lush or :LushRunTutorial

--
-- Note: Because this is lua file, vim will append your file to the runtime,
--       which means you can require(...) it in other lua code (this is useful),
--       but you should also take care not to conflict with other libraries.
--
--       (This is a lua quirk, as it has somewhat poor support for namespacing.)
--
--       Basically, name your file,
--
--       "super_theme/lua/lush_theme/super_theme_dark.lua",
--
--       not,
--
--       "super_theme/lua/dark.lua".
--
--       With that caveat out of the way...
--

-- Enable lush.ify on this file, run:
--
--  `:Lushify`
--
--  or
--
--  `:lua require('lush').ify()`

local lush = require('lush')
local hsl = lush.hsl

local C = {
  Background = {
    default  = hsl("#000000"),
    emphasis = hsl("#292929"),
    muted    = hsl("#1c1c1c"),
    status   = hsl("#875f5f"),
  },
  Foreground = {
    default  = hsl("#d9d9d9"),
    emphasis = hsl("#ffffff"),
    muted    = hsl("#3a3a3a"),
    status   = hsl("#d7d7af"),
  },
  Special = {
    comment  = hsl("#719872"),
    visual   = hsl("#005f5f"),
    visualEm = hsl("#004747"),
    warning  = hsl("#d7005f"),
    search   = hsl("#005f87"),
    menu     = hsl("#ffd7d7"),
    menuSel  = hsl("#87005f"),
  },
  Base = {
    black0     = hsl("#4e4e4e"),
    black1     = hsl("#626262"),

    white0     = hsl("#d0d0d0"),
    white1     = hsl("#e4e4e4"),

    red0       = hsl("#d68787"),
    red1       = hsl("#d75f87"),

    blue0      = hsl("#85add4"),
    blue1      = hsl("#add4fb"),

    green0     = hsl("#5f865f"),
    green1     = hsl("#87af87"),

    palegreen0 = hsl("#DFDEBD"),
    palegreen1 = hsl("#b3b061"),

    yellow0    = hsl("#d8af5f"),
    yellow1    = hsl("#ffd787"),

    magenta0   = hsl("#d7afaf"),
    magenta1   = hsl("#ffafaf"),

    cyan0      = hsl("#87afaf"),
    cyan1      = hsl("#87d7d7"),

    orange0    = hsl("#E19972"),
    orange1    = hsl("#d7875f"),

    brown0     = hsl("#af875f"),
    brown1     = hsl("#876543"),

    purple0    = hsl("#8787af"),
    purple1    = hsl("#8787ff"),


  }
}

-- LSP/Linters mistakenly show `undefined global` errors in the spec, they may
-- support an annotation like the following. Consult your server documentation.
---@diagnostic disable: undefined-global
local theme = lush(function(injected_functions)
  local sym = injected_functions.sym
  return {
    -- The following are all the Neovim default highlight groups from the docs
    -- as of 0.5.0-nightly-446, to aid your theme creation. Your themes should
    -- probably style all of these at a bare minimum.
    --
    -- Referenced/linked groups must come before being referenced/lined,
    -- so the order shown ((mostly) alphabetical) is likely
    -- not the order you will end up with.
    --
    -- You can uncomment these and leave them empty to disable any
    -- styling for that group (meaning they mostly get styled as Normal)
    -- or leave them commented to apply vims default colouring or linking.

    Comment { fg = C.Special.comment },                                   -- any comment
    ColorColumn { fg = C.Foreground.default },                            -- used for the columns set with 'colorcolumn'
    Conceal { fg = C.Foreground.default },                                -- placeholder characters substituted for concealed text (see 'conceallevel')
    Cursor { fg = C.Background.default, bg = C.Foreground.default },      -- character under the cursor
    lCursor { fg = C.Background.emphasis, bg = C.Foreground.emphasis },   -- the character under the cursor when |language-mapping| is used (see 'guicursor')
    CursorIM { fg = C.Foreground.default, bg = C.Foreground.muted },      -- like Cursor, but used when in IME mode |CursorIM|
    CursorColumn { fg = C.Foreground.default, bg = C.Background.muted },  -- Screen-column at the cursor, when 'cursorcolumn' is set.
    CursorLine { bg = C.Background.muted },                               -- Screen-line at the cursor, when 'cursorline' is set.  Low-priority if foreground (ctermfg OR guifg) is not set.
    Directory { fg = C.Base.palegreen0 },                                 -- directory names (and other special names in listings)
    DiffAdd { fg = C.Base.green1 },                                       -- diff mode: Added line |diff.txt|
    DiffChange { fg = C.Base.yellow0 },                                   -- diff mode: Changed line |diff.txt|
    DiffDelete { fg = C.Base.red1 },                                      -- diff mode: Deleted line |diff.txt|
    DiffText { fg = C.Base.green0, bg = C.Base.yellow0 },                 -- diff mode: Changed text within a changed line |diff.txt|
    EndOfBuffer { fg = C.Background.muted, bg = C.Background.default },   -- filler lines (~) after the end of the buffer.  By default, this is highlighted like |hl-NonText|.
    TermCursor { fg = C.Background.default, bg = C.Foreground.emphasis }, -- cursor in a focused terminal
    TermCursorNC { bg = C.Foreground.muted },                             -- cursor in an unfocused terminal
    ErrorMsg { fg = C.Base.magenta1 },                                    -- error messages on the command line
    VertSplit { fg = C.Background.muted },                                -- the column separating vertically split windows
    Folded { fg = '#87875f' },                                            -- line used for closed folds
    FoldColumn { fg = '#afaf87' },                                        -- 'foldcolumn'
    SignColumn { fg = '#d7875f' },                                        -- column where |signs| are displayed
    IncSearch { fg = C.Foreground.default, bg = C.Special.search },       -- 'incsearch' highlighting; also used for the text replaced with ":s///c"
    Substitute { fg = C.Background.default, bg = C.Special.comment },     -- |:substitute| replacement text highlighting
    LineNr { fg = "#999872", bg = C.Background.default },                 -- Line number for ":number" and ":#" commands, and when 'number' or 'relativenumber' option is set.
    CursorLineNr { fg = "#be7572" },                                      -- Like LineNr when 'cursorline' or 'relativenumber' is set for the cursor line.
    MatchParen { bg = C.Foreground.muted },                               -- The character under the cursor or just before it, if it is a paired bracket, and its match. |pi_paren.txt|
    ModeMsg { fg = C.Base.orange1 },                                      -- 'showmode' message (e.g., "-- INSERT -- ")
    MsgArea { fg = C.Foreground.default },                                -- Area for messages and cmdline
    MsgSeparator { fg = C.Foreground.status, bg = C.Background.status },  -- Separator for scrolled messages, `msgsep` flag of 'display'
    MoreMsg { fg = C.Base.green0, gui = "bold" },                         -- |more-prompt|
    NonText { fg = C.Foreground.muted, bg = C.Background.default },       -- '@' at the end of the window, characters from 'showbreak' and other characters that do not really exist in the text (e.g., ">" displayed when a double-wide character doesn't fit at the end of the line). See also |hl-EndOfBuffer|.
    Normal { fg = C.Foreground.default, bg = C.Background.default },      -- normal text
    NormalFloat { bg = C.Background.default },                            -- Normal text in floating windows.
    FloatBorder { fg = C.Background.emphasis },                           -- Popup menu: Border colour
    NormalNC { bg = C.Background.deafult },                               -- normal text in non-current windows

    Pmenu { fg = C.Foreground.default, bg = C.Background.muted },         -- Popup menu: normal item.
    PmenuSel { bg = C.Special.visual },                                   -- Popup menu: selected item.
    PmenuSbar { fg = C.Background.default, bg = C.Base.green0 },          -- Popup menu: scrollbar.
    PmenuThumb { bg = C.Background.emphasis },                            -- Popup menu: Thumb of the scrollbar.


    Question { fg = C.Base.yellow0 },                                      -- |hit-enter| prompt and yes/no questions
    QuickFixLine { bg = C.Special.visualEm },                              -- Current |quickfix| item in the quickfix window. Combined with |hl-CursorLine| when the cursor is there.
    Search { fg = C.Foreground.default, bg = C.Special.search },           -- Last search pattern highlighting (see 'hlsearch').  Also used for similar items that need to stand out.
    SpecialKey { fg = C.Foreground.muted, bg = C.Background.muted },       -- Unprintable characters: text displayed differently from what it really is.  But not 'listchars' whitespace. |hl-Whitespace|
    SpellBad { fg = C.Base.magenta0 },                                     -- Word that is not recognized by the spellchecker. |spell| Combined with the highlighting used otherwise.
    SpellCap { fg = C.Base.magenta0 },                                     -- Word that should start with a capital. |spell| Combined with the highlighting used otherwise.
    SpellLocal { fg = C.Base.magenta1 },                                   -- Word that is recognized by the spellchecker as one that is used in another region. |spell| Combined with the highlighting used otherwise.
    SpellRare { fg = C.Base.yellow0 },                                     -- Word that is recognized by the spellchecker as one that is hardly ever used.  |spell| Combined with the highlighting used otherwise.
    StatusLine { fg = C.Foreground.status, bg = C.Background.status },     -- status line of current window
    StatusLineNC { fg = C.Foreground.status, bg = C.Background.emphasis }, -- status lines of not-current windows Note: if this is equal to "StatusLine" Vim will use "^^^" in the status line of the current window.
    TabLine { fg = C.Base.green0, bg = C.Background.muted },               -- tab pages line, not active tab page label
    TabLineFill { fg = C.Background.default, bg = C.Base.green0 },         -- tab pages line, where there are no labels
    TabLineSel { fg = C.Base.green0, gui = "bold" },                       -- tab pages line, active tab page label
    Title { fg = C.Base.magenta1 },                                        -- titles for output from ":set all", ":autocmd" etc.
    Visual { bg = C.Special.visual },                                      -- Visual mode selection
    VisualNOS { bg = C.Special.visualEm },                                 -- Visual mode selection when vim is "Not Owning the Selection".
    WarningMsg { fg = C.Special.warning },                                 -- warning messages
    Whitespace { fg = C.Background.muted, bg = C.Background.default },     -- "nbsp", "space", "tab" and "trail" in 'listchars'
    WildMenu { fg = C.Background.default, bg = C.Base.yellow0 },           -- current match in 'wildmenu' completion
    WinSeparator { fg = C.Background.default, bg = C.Background.default },

    -- These groups are not listed as default vim groups,
    -- but they are defacto standard group names for syntax highlighting.
    -- commented out groups should chain up to their "preferred" group by
    -- default,
    -- Uncomment and edit if you want more specific syntax highlighting.

    Constant { fg = C.Base.cyan0 },        -- (preferred) any constant
    String { fg = C.Base.cyan0 },          --   a string constant: "this is a string"
    Character { fg = C.Base.red0 },        --  a character constant: 'c', '\n'
    Number { fg = C.Base.yellow1 },        --   a number constant: 234, 0xff
    Boolean { fg = C.Base.purple0 },       --  a boolean constant: TRUE, false
    Float { fg = C.Base.yellow1 },         --    a floating point constant: 2.3e10

    Identifier { fg = C.Base.magenta1 },   -- (preferred) any variable name
    Function { fg = C.Base.red0 },         -- function name (also: methods for classes)

    Statement { fg = C.Base.green1 },      -- (preferred) any statement
    Conditional { fg = C.Base.red1 },      --  if, then, else, endif, switch, etc.
    Repeat { fg = C.Base.red1 },           --   for, do, while, etc.
    Label { fg = C.Base.red0 },            --    case, default, etc.
    Operator { fg = C.Base.palegreen0 },   -- "sizeof", "+", "*", etc.
    Keyword { fg = C.Base.red1 },          --  any other keyword
    Exception { fg = C.Base.magenta1 },    --  try, catch, throw

    PreProc { fg = C.Base.green0 },        -- (preferred) generic Preprocessor
    Include { fg = C.Base.orange1 },       --  preprocessor #include
    Define { fg = C.Base.orange1 },        --   preprocessor #define
    Macro { fg = C.Base.orange1 },         --    same as Define
    PreCondit { fg = C.Base.orange1 },     --  preprocessor #if, #else, #endif, etc.

    Type { fg = C.Base.yellow0 },          -- (preferred) int, long, char, etc.
    StorageClass { fg = C.Base.yellow0 },  -- static, register, volatile, etc.
    Structure { fg = C.Base.yellow0 },     --  struct, union, enum, etc.
    Typedef { fg = C.Base.blue0 },         --  A typedef

    Special { fg = C.Base.orange1 },       -- (preferred) any special symbol
    SpecialChar { fg = C.Base.orange0 },   --  special character in a constant
    Tag { fg = C.Base.orange1 },           --    you can use CTRL-] on this
    Delimiter { fg = C.Base.brown0 },      --  character that needs attention
    SpecialComment { fg = C.Base.brown0 }, -- special things inside a comment
    Debug { fg = C.Base.brown0 },          --    debugging statements

    Underlined { gui = "underline" },      -- (preferred) text that stands out, HTML links
    Bold { gui = "bold" },
    Italic { gui = "italic" },

    -- ("Ignore", below, may be invisible...)
    -- Ignore         { }, -- (preferred) left blank, hidden  |hl-Ignore|

    Error { fg = C.Special.warning }, -- (preferred) any erroneous construct
    Todo { fg = C.Base.blue0 },       -- (preferred) anything that needs extra attention; mostly the keywords TODO FIXME and XXX

    -- These groups are for the native LSP client and diagnostic system. Some
    -- other LSP clients may use these groups, or use their own. Consult your
    -- LSP client's documentation.

    -- See :h lsp-highlight, some groups may not be listed, submit a PR fix to lush-template!
    --
    LspReferenceText { bg = C.Background.muted },                                      -- used for highlighting "text" references
    LspReferenceRead { bg = C.Background.muted },                                      -- used for highlighting "read" references
    LspReferenceWrite { bg = C.Background.muted },                                     -- used for highlighting "write" references

    LspCodeLens { fg = C.Foreground.muted, bg = C.Background.muted },                  -- Used to color the virtual text of the codelens. See |nvim_buf_set_extmark()|.
    LspCodeLensSeparator { fg = C.Base.brown0, bg = C.Base.brown0 },                   -- Used to color the seperator between two or more code lens.
    LspSignatureActiveParameter { fg = C.Background.default, bg = C.Special.comment }, -- Used to highlight the active parameter in the signature help. See |vim.lsp.handlers.signature_help()|.

    -- https://github.com/hrsh7th/nvim-cmp/wiki/Menu-Appearance#how-to-get-types-on-the-left-and-offset-the-menu
    -- At the bottom of this page are a list of hilightgroups

    -- TODO: get these colours right
    -- CmpItemAbbrDeprecated = { fg = "#7E8294", bg = "NONE", strikethrough = true },
    -- CmpItemAbbrMatch = { fg = "#82AAFF", bg = "NONE", bold = true },
    -- CmpItemAbbrMatchFuzzy = { fg = "#82AAFF", bg = "NONE", bold = true },
    -- CmpItemMenu = { fg = "#C792EA", bg = "NONE", italic = true },

    -- CmpItemKindField = { fg = "#EED8DA", bg = "#B5585F" },
    -- CmpItemKindProperty = { fg = "#EED8DA", bg = "#B5585F" },
    -- CmpItemKindEvent = { fg = "#EED8DA", bg = "#B5585F" },

    -- CmpItemKindText = { fg = "#C3E88D", bg = "#9FBD73" },
    -- CmpItemKindEnum = { fg = "#C3E88D", bg = "#9FBD73" },
    -- CmpItemKindKeyword = { fg = "#C3E88D", bg = "#9FBD73" },

    -- CmpItemKindConstant = { fg = "#FFE082", bg = "#D4BB6C" },
    -- CmpItemKindConstructor = { fg = "#FFE082", bg = "#D4BB6C" },
    -- CmpItemKindReference = { fg = "#FFE082", bg = "#D4BB6C" },

    -- CmpItemKindFunction = { fg = "#EADFF0", bg = "#A377BF" },
    -- CmpItemKindStruct = { fg = "#EADFF0", bg = "#A377BF" },
    -- CmpItemKindClass = { fg = "#EADFF0", bg = "#A377BF" },
    -- CmpItemKindModule = { fg = "#EADFF0", bg = "#A377BF" },
    -- CmpItemKindOperator = { fg = "#EADFF0", bg = "#A377BF" },

    -- CmpItemKindVariable = { fg = "#C5CDD9", bg = "#7E8294" },
    -- CmpItemKindFile = { fg = "#C5CDD9", bg = "#7E8294" },

    -- CmpItemKindUnit = { fg = "#F5EBD9", bg = "#D4A959" },
    -- CmpItemKindSnippet = { fg = "#F5EBD9", bg = "#D4A959" },
    -- CmpItemKindFolder = { fg = "#F5EBD9", bg = "#D4A959" },

    -- CmpItemKindMethod = { fg = "#DDE5F5", bg = "#6C8ED4" },
    -- CmpItemKindValue = { fg = "#DDE5F5", bg = "#6C8ED4" },
    -- CmpItemKindEnumMember = { fg = "#DDE5F5", bg = "#6C8ED4" },

    -- CmpItemKindInterface = { fg = "#D8EEEB", bg = "#58B5A8" },
    -- CmpItemKindColor = { fg = "#D8EEEB", bg = "#58B5A8" },
    -- CmpItemKindTypeParameter = { fg = "#D8EEEB", bg = "#58B5A8" },


    CmpItemAbbrDeprecated { bg = 'NONE', strikethrough = true, fg = '#808080' },

    CmpItemAbbrMatch { fg = 'NONE', bg = 'NONE' },
    CmpItemAbbrMatchFuzzy { link = 'CmpIntemAbbrMatch' },

    CmpItemKindVariable { fg = C.Base.magenta1 },
    CmpItemKindInterface { link = 'CmpItemKindVariable' },
    CmpItemKindText { link = 'CmpItemKindVariable' },

    CmpItemKindFunction { fg = C.Base.yellow1 },
    CmpItemKindMethod { link = 'CmpItemKindFunction' },

    CmpItemKindKeyword { fg = C.Base.red1 },
    CmpItemKindProperty { link = 'CmpItemKindKeyword' },
    CmpItemKindUnit { link = 'CmpItemKindKeyword' },


    -- See :h diagnostic-highlights, some groups may not be listed, submit a PR fix to lush-template!
    --
    DiagnosticError { fg = C.Base.magenta0 }, -- Used as the base highlight group. Other Diagnostic highlights link to this by default (except Underline)
    DiagnosticWarn { fg = C.Base.yellow0 },   -- Used as the base highlight group. Other Diagnostic highlights link to this by default (except Underline)
    DiagnosticInfo { fg = C.Base.blue0 },     -- Used as the base highlight group. Other Diagnostic highlights link to this by default (except Underline)
    DiagnosticHint { fg = C.Base.brown0 },    -- Used as the base highlight group. Other Diagnostic highlights link to this by default (except Underline)
    -- DiagnosticVirtualTextError { } , -- Used for "Error" diagnostic virtual text.
    -- DiagnosticVirtualTextWarn  { } , -- Used for "Warn" diagnostic virtual text.
    -- DiagnosticVirtualTextInfo  { } , -- Used for "Info" diagnostic virtual text.
    -- DiagnosticVirtualTextHint  { } , -- Used for "Hint" diagnostic virtual text.
    -- DiagnosticUnderlineError   { } , -- Used to underline "Error" diagnostics.
    -- DiagnosticUnderlineWarn    { } , -- Used to underline "Warn" diagnostics.
    -- DiagnosticUnderlineInfo    { } , -- Used to underline "Info" diagnostics.
    -- DiagnosticUnderlineHint    { } , -- Used to underline "Hint" diagnostics.
    -- DiagnosticFloatingError    { } , -- Used to color "Error" diagnostic messages in diagnostics float. See |vim.diagnostic.open_float()|
    -- DiagnosticFloatingWarn     { } , -- Used to color "Warn" diagnostic messages in diagnostics float.
    -- DiagnosticFloatingInfo     { } , -- Used to color "Info" diagnostic messages in diagnostics float.
    -- DiagnosticFloatingHint     { } , -- Used to color "Hint" diagnostic messages in diagnostics float.
    -- DiagnosticSignError        { } , -- Used for "Error" signs in sign column.
    -- DiagnosticSignWarn         { } , -- Used for "Warn" signs in sign column.
    -- DiagnosticSignInfo         { } , -- Used for "Info" signs in sign column.
    -- DiagnosticSignHint         { } , -- Used for "Hint" signs in sign column.


    -- Tree-Sitter syntax groups.
    --
    -- See :h treesitter-highlight-groups, some groups may not be listed,
    -- submit a PR fix to lush-template!
    --
    -- Tree-Sitter groups are defined with an "@" symbol, which must be
    -- specially handled to be valid lua code, we do this via the special
    -- sym function. The following are all valid ways to call the sym function,
    -- for more details see https://www.lua.org/pil/5.html
    --
    -- sym("@text.literal")
    -- sym('@text.literal')
    -- sym"@text.literal"
    -- sym'@text.literal'
    --
    -- For more information see https://github.com/rktjmp/lush.nvim/issues/109

    sym "@text.literal" { fg = C.Special.comment },           -- Comment
    sym "@text.reference" { fg = C.Base.brown0 },             -- Identifier
    sym "@text.title" { fg = C.Base.magenta1 },               -- Title
    sym "@text.uri" { fg = C.Base.blue0, gui = "underline" }, -- Underlined
    sym "@text.underline" { gui = "underline" },              -- Underlined
    sym "@text.todo" { fg = C.Base.blue0 },                   -- Todo
    sym "@comment" { fg = C.Special.comment },                -- Comment
    sym "@punctuation" { fg = C.Base.brown0 },                -- Delimiter
    sym "@constant" { fg = C.Base.cyan0 },                    -- Constant
    sym "@constant.builtin" { fg = C.Base.yellow0 },          -- Special
    sym "@constant.macro" { fg = C.Base.orange0 },            -- Define
    sym "@define" { fg = C.Base.orange1 },                    -- Define
    sym "@macro" { fg = C.Base.orange1 },                     -- Macro
    sym "@string" { fg = C.Base.cyan0 },                      -- String
    sym("@string.escape") { fg = C.Base.red1 },               -- SpecialChar
    -- sym"@string.special"    { }, -- SpecialChar
    -- sym"@character"         { }, -- Character
    -- sym"@character.special" { }, -- SpecialChar
    -- sym"@number"            { }, -- Number
    -- sym"@boolean"           { }, -- Boolean
    -- sym"@float"             { }, -- Float
    -- sym"@function"          { }, -- Function
    -- sym"@function.builtin"  { }, -- Special
    -- sym"@function.macro"    { }, -- Macro
    -- sym"@parameter"         { }, -- Identifier
    -- sym"@method"            { }, -- Function
    -- sym"@field"             { }, -- Identifier
    -- sym"@property"          { }, -- Identifier
    -- sym"@constructor"       { }, -- Special
    -- sym"@conditional"       { }, -- Conditional
    -- sym"@repeat"            { }, -- Repeat
    -- sym"@label"             { }, -- Label
    -- sym"@operator"          { }, -- Operator
    -- sym"@keyword"           { }, -- Keyword
    -- sym"@exception"         { }, -- Exception
    -- sym"@variable"          { }, -- Identifier
    -- sym"@type"              { }, -- Type
    -- sym"@type.definition"   { }, -- Typedef
    -- sym"@storageclass"      { }, -- StorageClass
    -- sym"@structure"         { }, -- Structure
    -- sym"@namespace"         { }, -- Identifier
    -- sym"@include"           { }, -- Include
    -- sym"@preproc"           { }, -- PreProc
    -- sym"@debug"             { }, -- Debug
    -- sym"@tag"               { }, -- Tag



    -- TreeSitter

    sym("@property") { fg = C.Base.magenta0 },

    sym("@tag") { fg = C.Base.green1 },
    sym("@tag.delimiter") { fg = C.Base.brown0 },
    sym("@tag.attribute") { fg = C.Base.yellow0 },

    sym("@variable") { fg = C.Base.magenta1 },
    sym("@parameter") { fg = C.Base.magenta0 },

    -- sym("@text.title")            { fg = C.Base.magenta1 },
    sym("@punctuation.special") { fg = C.Base.brown0 },

    sym("@method.call") { fg = C.Base.magenta0 },

    sym("@function") { fg = C.Base.white1 },
    sym("@function.call") { fg = C.Base.white0 },
    sym("@function.builtin") { fg = C.Base.yellow0 },

    sym("@include") { fg = C.Base.palegreen1 },

    sym("@operator") { fg = C.Base.palegreen1 },

    sym("@conditional") { fg = C.Base.red1 },
    sym("@keyword") { fg = C.Base.red1 },
    sym("@keyword.global") { fg = C.Base.yellow0 },
    sym("@keyword.return") { fg = C.Base.green0 },
    sym("@keyword.function") { fg = C.Base.red1 },
    sym("@repeat") { fg = C.Base.blue0 },
    sym("@type") { fg = C.Base.blue0 },
    sym("@type.qualifier") { fg = C.Base.yellow0 },
    sym("@constructor") { fg = C.Base.brown1 },
    sym("@field") { fg = C.Base.palegreen1 },


    sym("@variable.javascript") { fg = C.Base.magenta1 },
    sym("@variable.builtin.javascript") { fg = C.Base.blue0 },
    sym("@method.call.javascript") { fg = C.Base.white0 },
    sym("@include.javascript") { fg = C.Base.orange1 },
    sym("@conditional.javascript") { fg = C.Base.blue0 },
    sym("@function.call.javascript") { fg = C.Base.palegreen0 },
    sym("@keyword.javascript") { fg = C.Base.yellow0 },

    sym("@function.method.vue") { fg = C.Base.red1 },

    sym("@label.json") { fg = C.Base.green1 },

    sym("@keyword.lua") { fg = C.Base.green1 },
    sym("@function.builtin.lua") { fg = C.Base.orange0 },
    sym("@field.lua") { fg = C.Base.magenta0 },
    sym("@function.lua") { fg = C.Base.yellow0 },

    sym("@text.uri.gitcommit") { fg = C.Base.blue0 },
    sym("@text.reference.gitcommit") { fg = C.Base.brown0 },

    sym("@property.scss") { fg = C.Base.yellow0 },
    sym("@type.scss") { fg = C.Base.magenta1 },
    sym("@function.scss") { fg = C.Base.palegreen0 },
    sym("@variable.scss") { fg = C.Base.magenta0 },
    sym("@tag.attribute.scss") { fg = C.Base.cyan0 },
    sym("@keyword.import.scss") { fg = C.Base.green0 },

    sym("@property.json") { fg = C.Base.green1 },

    sym("@constant.bash") { fg = C.Base.magenta0 },

    sym("@markup.heading") { fg = C.Base.magenta1 },

    sym("@string.special.url") { fg = C.Base.blue0 },
    sym("@string.special.url.gitcommit") { fg = C.Base.cyan0 },
    sym("@string.special.path.gitcommit") { fg = C.Base.cyan0 },

    sym("@keyword.gitcommit") { fg = C.Base.yellow0 },
    sym("@keyword.import") { fg = C.Base.orange0 },
    sym("@keyword.import.php") { fg = C.Base.palegreen1 },

    sym("@markup.link.url") { fg = C.Base.yellow0 },
    sym("@markup.link.label") { fg = C.Base.cyan0 },
    sym("@markup.list") { fg = C.Base.magenta0 },
    sym("@markup.strong") { fg = C.Base.white1, gui = "bold" },

    mailQuoted1 { fg = C.Base.green0 },
    mailQuoted2 { fg = C.Base.green1 },
    mailQuoted3 { fg = C.Base.blue0 },
    mailQuoted4 { fg = C.Base.blue1 },
    mailQuoted5 { fg = C.Base.cyan1 },
    mailQuoted6 { fg = C.Base.cyan0 },

    sym("@string.special.path.gitignore") { fg = C.Base.cyan0 },


    -- Fallbacks

    SassMixinName { fg = C.Base.palegreen0 },
    scssFunctionName { fg = C.Base.palegreen0 },
    sassClass { fg = C.Base.magenta1 },
    sassDefinition { fg = C.Base.cyan1 },

    cssTextProp { fg = C.Base.yellow0 },
    cssBraces { fg = C.Base.brown1 },
    cssBackgroundProp { fg = C.Base.yellow0 },
    cssTransformProp { fg = C.Base.yellow0 },


    DashboardShortCut { fg = C.Base.magenta0 },
    DashboardHeader { fg = C.Base.blue0 },
    DashboardCenter { fg = C.Base.green0 },
    DashboardFooter { fg = C.Base.yellow0, style = { "italic" } },
    DashboardMruTitle { fg = C.Base.blue1 },
    DashboardProjectTitle { fg = C.Base.blue1 },
    DashboardFiles { fg = C.Base.purple0 },
    DashboardKey { fg = C.Base.magenta1 },
    DashboardDesc { fg = C.Base.blue0 },
    DashboardIcon { fg = C.Base.magenta0, bold = true },

    DiagnosticVirtualTextInfo { fg = C.Base.blue0 },
    DiagnosticVirtualTextHint { fg = C.Base.blue0 },
    DiagnosticVirtualTextWarn { fg = C.Base.blue0 },


  }
end)

-- return our parsed theme for extension or use else where.
return theme

-- vi:nowrap
