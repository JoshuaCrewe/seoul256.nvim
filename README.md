# Seoul256 (A Lush Theme for Neovim).

![Vim home page in Seoul256 colours](/assets/screenshots/home.png)

See: http://git.io/lush.nvim for more information on Lush and a helper script
to setup your repo clone.

## Screenshots

Hi Joe 👋

Treesitter turned out to be a bit limited with what could be targeted. These screenshots are using regex which are extended by some specific language plugins.

![PHP code highlighted in Seoul256 colours](/assets/screenshots/php.png)
The PHP front page from a Wordpress project.

![CSS and JS code highlighted in Seoul256 colours](/assets/screenshots/css-js-error.png)

CSS and JS code with an error in the status bar
